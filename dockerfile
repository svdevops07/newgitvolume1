FROM nginx

RUN  mkdir /var/www

COPY default.conf  /etc/nginx/conf.d/

VOLUME /var/www
